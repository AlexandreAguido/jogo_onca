const CircleX = 80, CircleY = 100, circleRadius = 50,
    squareSide = 120, MARGIN_TOP = 40, MARGIN_LEFT = 40,
    LINHAS = 5, COLUNAS = 5, CANVAS_WIDTH = 800, CANVAS_HEIGHT = 800,
    IMG_DIAMETRO = 50, POSSIBLE_MOVES_POINTS = [], MOVE_POINT_DIAMETRO = 24
SELECIONADO = [], SERVER_URL = 'http://localhost:3000'
let ehCachorro, meu_turno, dog_img, onca_img, fundo_img, startGame = false, mapearPosicaoPecas

// const BOARD_STATE = [
//     /*
//     O = onça
//     C = cachorro
//     . = ponto vazio
//     | = ponto inválido 
//     */
//     ['C', 'C', 'C', 'C', 'C'],
//     ['C', 'C', 'O', 'C', 'C'],
//     ['.', '.', '.', '.', '.'],
//     ['.', '.', '.', '.', '.'],
//     ['.', '.', '.', '.', '.'],
// ]
let BOARD_STATE = []
const DIRECOES = [
    //(x,y)
    [0, -1], //CIMA
    [1, 0], //DIREITA
    [0, 1], //BAIXO
    [-1, 0] //ESQUERDA
]
const DIAGONAIS = [
    //(x,y)
    [1, -1], //NORDESTE
    [1, 1], //SUDESTE
    [-1, 1], //SUDOESTE
    [-1, -1] //NOROESTE
]
let POS_VETOR_TABULEIRO = {}
const PONTOS_DO_TRIANGULO = {
    '[1,5]': [1.2 * squareSide + MARGIN_LEFT, 4.8 * squareSide + MARGIN_TOP],
    '[2,5]': [2 * squareSide + MARGIN_TOP, 4.8 * squareSide + MARGIN_TOP],
    '[3,5]': [2.8 * squareSide + MARGIN_LEFT, 4.8 * squareSide + MARGIN_TOP],
    '[1,6]': [0.5 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP],
    '[2,6]': [2 * squareSide + MARGIN_TOP, 5.5 * squareSide + MARGIN_TOP],
    '[3,6]': [3.5 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP]
}

function preload() {
    dog_img = loadImage('../img/dog.png')
    onca_img = loadImage('../img/onca.png')
    fundo = loadImage('../img/fundo.png')

}
function setup() {
    createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT)
    noLoop()
}

function draw() {
    if (startGame && BOARD_STATE != []) {
        strokeWeight(1)
        stroke('black')
        fill('rgba(0,0,0,0)')
        background(fundo)
        desenharQuadrados()
        desenharDiagonais()
        desenharPecas()
        stroke(`${ehCachorro ? 'yellow' : 'green'}`)
        strokeWeight(MOVE_POINT_DIAMETRO)
        POSSIBLE_MOVES_POINTS.forEach(element => {
            if (element[1] > 4) {
                let posicaoNoMapa = PONTOS_DO_TRIANGULO[JSON.stringify(element)]
                try {
                    point(posicaoNoMapa[0], posicaoNoMapa[1])

                } catch (error) {
                    console.warn(posicaoNoMapa, element, JSON.stringify(element), PONTOS_DO_TRIANGULO)
                }
            }
            else {
                point(element[0] * squareSide + MARGIN_LEFT, element[1] * squareSide + MARGIN_TOP)
            }
        })

    }
}

function desenharDiagonais() {
    line(MARGIN_LEFT, MARGIN_TOP, 4 * squareSide + MARGIN_LEFT, 4 * squareSide + MARGIN_TOP)
    line(4 * squareSide + MARGIN_LEFT, MARGIN_TOP, MARGIN_LEFT, 4 * squareSide + MARGIN_TOP)
    line(MARGIN_LEFT + 2 * squareSide, MARGIN_TOP, MARGIN_LEFT, 2 * squareSide + MARGIN_TOP)
    line(MARGIN_LEFT + 2 * squareSide, MARGIN_TOP, 4 * squareSide + MARGIN_LEFT, 2 * squareSide + MARGIN_TOP)
    line(MARGIN_LEFT, 2 * squareSide + MARGIN_TOP, 3.5 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP)
    line(4 * squareSide + MARGIN_LEFT, 2 * squareSide + MARGIN_TOP, 0.5 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP)
    line(1.2 * squareSide + MARGIN_LEFT, 4.8 * squareSide + MARGIN_TOP, 2.8 * squareSide + MARGIN_LEFT, 4.8 * squareSide + MARGIN_TOP)
    line(0.5 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP, 3.5 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP)
    line(2 * squareSide + MARGIN_LEFT, 4 * squareSide + MARGIN_TOP, 2 * squareSide + MARGIN_LEFT, 5.5 * squareSide + MARGIN_TOP)

}

function desenharQuadrados() {
    for (i = 0; i < COLUNAS - 1; i++) {
        for (j = 0; j < LINHAS - 1; j++) {
            square(MARGIN_TOP + i * squareSide, MARGIN_LEFT + j * squareSide, squareSide)
        }
    }
}

function desenharPecas() {
    let img_x, img_y
    if (mapearPosicaoPecas) POS_VETOR_TABULEIRO = {}
    for (i = 0; i < COLUNAS; i++) {
        for (j = 0; j < LINHAS + 2; j++) {
            if (j > 4 && PONTOS_DO_TRIANGULO[JSON.stringify([i, j])]) {
                aux = PONTOS_DO_TRIANGULO[JSON.stringify([i, j])]
                img_x = aux[0] - IMG_DIAMETRO
                img_y = aux[1] - IMG_DIAMETRO
            }
            else {
                img_x = i * squareSide
                img_y = j * squareSide
            }
            switch (BOARD_STATE[j][i]) {
                case 'O':
                    image(onca_img, img_x, img_y)
                    POS_VETOR_TABULEIRO[JSON.stringify([i, j])] = [img_x, img_y]
                    break
                case 'C':
                    image(dog_img, img_x, img_y)
                    POS_VETOR_TABULEIRO[JSON.stringify([i, j])] = [img_x, img_y]
                    break
            }
        }
    }
    mapearPosicaoPecas = false
}

function isValidMove(x, y) {
    return x >= 0 && x < COLUNAS && y >= 0 && y < LINHAS + 2 &&
        BOARD_STATE[y][x] != '|'
}

function movimentoInvalidoNoTriangulo(x, y, novoX, novoY) {
    // filtrar movimentos invalidos a partir das posicoes (x,y) [1,5] e [3,5]
    if (x == 1 && y == 5) {
        if (!['[1,6]', '[2,4]', '[2,5]'].includes(JSON.stringify([novoX, novoY]))) return true
    }
    if (x == 3 && y == 5) {
        if (!['[3,6]', '[2,4]', '[2,5]'].includes(JSON.stringify([novoX, novoY]))) return true
    }
    if (y == 4 && x != 2) return novoY > 4
    return false
}

function selecionarPeca() {
    let clicouNaImagem = false
    Object.entries(POS_VETOR_TABULEIRO).some(item => {
        aux = item[0].replace('{', '').replace
        if (mouseX >= item[1][0] && mouseX <= item[1][0] + IMG_DIAMETRO &&
            mouseY >= item[1][1] && mouseY <= item[1][1] + IMG_DIAMETRO) {
            clicouNaImagem = true
            aux = item[0].replace(']', '').replace('[', '').split(',')
            x = +aux[0]
            y = +aux[1]
            if (BOARD_STATE[y][x] != '.' && BOARD_STATE[y][x] != '|') {
                SELECIONADO.length = 0
                SELECIONADO.push([x, y])
                showPossibleMoves(x, y)
            }
            return true
        }
        return false
    })
}
function moverPeca() {
    if (meu_turno) {
        POSSIBLE_MOVES_POINTS.forEach(element => {
            x = element[0], y = element[1]
            let point_x, point_y
            old_x = SELECIONADO[0][0], old_y = SELECIONADO[0][1]
            keyPontosTriangulo = JSON.stringify(element)
            if (Object.keys(PONTOS_DO_TRIANGULO).includes(keyPontosTriangulo)) {
                point_x = PONTOS_DO_TRIANGULO[keyPontosTriangulo][0]
                point_y = PONTOS_DO_TRIANGULO[keyPontosTriangulo][1]
            }
            else {
                point_x = x * squareSide + MARGIN_LEFT
                point_y = y * squareSide + MARGIN_TOP
            }

            if (dist(point_x, point_y, mouseX, mouseY) <= MOVE_POINT_DIAMETRO) {
                socket.emit('move', { x, y, old_x, old_y })
            }
        })
    }
    POSSIBLE_MOVES_POINTS.length = 0
}
function showPossibleMoves(x, y) {
    if (!meu_turno) return
    POSSIBLE_MOVES_POINTS.length = 0
    posicao = BOARD_STATE[y][x]
    if (posicao == '.' || (!ehCachorro && posicao == 'C') || (ehCachorro && posicao == 'O')) return
    DIRECOES.forEach(el => {
        novoX = x + el[0]
        novoY = y + el[1]
        if (!isValidMove(novoX, novoY)) return
        if (movimentoInvalidoNoTriangulo(x, y, novoX, novoY)) return
        if (BOARD_STATE[novoY][novoX] == '.') POSSIBLE_MOVES_POINTS.push([novoX, novoY])
    })
    // diagonais 
    posicaoString = JSON.stringify([x, y])
    pecaPodeMoverDiagonal = ((5 * y + x) % 2 == 0 && y < 5) || ((y >= 5) && posicaoString != '[2,5]' && posicaoString != '[2,6]')
    if (pecaPodeMoverDiagonal) {
        DIAGONAIS.forEach(el => {
            novoX = x + el[0]
            novoY = y + el[1]
            if (!isValidMove(novoX, novoY)) return
            if (movimentoInvalidoNoTriangulo(x, y, novoX, novoY)) return
            if (BOARD_STATE[novoY][novoX] == '.') POSSIBLE_MOVES_POINTS.push([novoX, novoY])
        })
    }

}

function mouseClicked() {
    if (!meu_turno || BOARD_STATE.length == 0) return
    if (!POSSIBLE_MOVES_POINTS.length) selecionarPeca()
    else moverPeca()
}

function start() {
    startGame = true
    if (meu_turno) mudarMensagem('SUA VEZ!')
    else mudarMensagem('AGUARDANDO O OUTRO JOGADOR..')
    document.getElementById('animalSelecionado').innerText = `Jogando com ${ehCachorro ? 'o cachorro' : 'a onça'}`
    loop()
}

function mudarMensagem(msg) {
    document.getElementById("msg").innerText = msg
}
let socket = io(SERVER_URL);
socket.on('animal', function (data) {
    ehCachorro = data.animal ? 1 : 0
    meu_turno = !ehCachorro
})
socket.on('startGame', function (data) {
    BOARD_STATE = data.BOARD_STATE
    mapearPosicaoPecas = true
    setTimeout(start, 1000)
})

socket.on('playerMove', function (data) {
    BOARD_STATE = data.BOARD_STATE
    mapearPosicaoPecas = true

    meu_turno = ehCachorro == data.player_turn
    SELECIONADO.length = 0
    POSSIBLE_MOVES_POINTS.length = 0
    msg = ""
    if (meu_turno) {
        msg = 'SUA VEZ!'
    }
    else {
        msg = 'AGUARDANDO O OUTRO JOGADOR...'
    }
    setTimeout(mudarMensagem(msg), 1000)
})





