const express = require('express')
const http = require('http');
const { Server } = require("socket.io");
const app = express()
const server = http.createServer(app);
const io = new Server(server);
const BOARD_STATE = [
    ['C', 'C', 'C', 'C', 'C'],
    ['C', 'C', 'C', 'C', 'C'],
    ['C', 'C', 'O', 'C', 'C'],
    ['.', '.', '.', '.', '.'],
    ['.', '.', '.', '.', '.'],
    ['|', '.', '.', '.', '|'],
    ['|', '.', '.', '.', '|'],
]
app.use(express.static('frontend'))
let players = []
let player_turn = 0 // 0 onça, 1 cachorro


io.on('connection', socket => {
    if (players.length > 2) return
    players.push(socket.id)
    animal = players.length == 2 ? 1 : 0
    console.log(`player ${socket.id} adicionado, jogando com ${animal == 0 ? 'onça' : 'cachorro'}`)
    socket.emit('animal', { animal })
    if (players.length == 2) {
        io.emit('startGame', { BOARD_STATE })
    }
    socket.on('disconnect', () => {
        // fim de jogo
    })

    socket.on('move', data => {
        // TODO: validar movimento
        const { x, y, old_x, old_y } = data
        aux = BOARD_STATE[y][x]
        BOARD_STATE[y][x] = BOARD_STATE[old_y][old_x]
        BOARD_STATE[old_y][old_x] = aux
        player_turn = (player_turn + 1) % 2
        io.emit('playerMove', {
            BOARD_STATE, player_turn
        })
    })
})

server.listen(3000, () => {
    console.log('http://localhost:3000')
})